<?php
/**
 * @file
 * Body Classes Utility
 */
 
/**
 * OM Tools Node Utility Settings
 *
 */
function om_node(&$form) {
  drupal_add_js(drupal_get_path('module', 'om_tools') . '/tools/node/js/node.js', 'module');

  // Reset Node Form
  if ( variable_get('om_tools_node_tools_reset', 0)) {
    foreach (node_get_types() as $type => $info) {
      variable_set('om_tools_node_' . $type . '_titles', 0);
    }  
    variable_set('om_tools_node_body_classes', 1);
    variable_set('om_tools_node_body_classes_prefix', 'content-type-');
    variable_set('om_tools_node_terms_body_classes', 1); 
    variable_set('om_tools_node_terms_body_classes_prefix', 'content-term-');
    variable_set('om_tools_node_tools_reset', 0);
  }  
  // Node Form
  $form['node_tools'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Tools'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['node_tools']['node_tools_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Apply node tools'),
    '#default_value' => variable_get('om_tools_node_tools_switch', 1),
    '#description' => t(''),
  );  
  $form['node_tools']['node_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-node-settings'),
    '#title' => t('Body Classes Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );  
  $form['node_tools']['node_settings']['node_body_classes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add node body classes'),
    '#default_value' => variable_get('om_tools_node_body_classes', 1),
    '#description' => t(''),
  );  
  $form['node_tools']['node_settings']['node_body_classes_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Node class prefix'),
    '#default_value' => variable_get('om_tools_node_body_classes_prefix', 'content-type-'),
    '#description' => t('content-type-page content-type-page-add | content-type-page-edit | content-type-page-view | content-type-page-delete section-node-add | section-node-edit | section-node-delete'),
  ); 
  $form['node_tools']['node_settings']['node_terms_body_classes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add node terms body classes'),
    '#default_value' => variable_get('om_tools_node_terms_body_classes', 1),
    '#description' => t(''),
  );  
  $form['node_tools']['node_settings']['node_terms_body_classes_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Term class prefix'),
    '#default_value' => variable_get('om_tools_node_terms_body_classes_prefix', 'content-term-'),
    '#description' => t('content-term-mobile content-term-technology content-term-educaton ...'),
  );

  $form['node_tools']['node_titles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Titles'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Hide all titles for each content type.'),
  );
  
  foreach (node_get_types() as $type => $info) {
    $form['node_tools']['node_titles']['node_' . $type . '_titles'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide ' . $type . ' titles'),
      '#default_value' => variable_get('om_tools_node_' . $type . '_titles', 0),
      '#description' => t(''),
    );
  }
  
  $form['node_tools']['node_tools_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),      
    '#default_value' => variable_get('om_tools_node_tools_reset', 0),
    '#description' => t(''),
  );                   
  return $form;  
}

/**
 * Override or insert om variables into the templates.
 */
function om_node_preprocess_page(&$vars) {
  if (variable_get('om_tools_node_tools_switch', 1)) {
    $classes = explode(' ', $vars['body_classes']);
    if (!$vars['is_front']) {
      // Add unique class for each page.
      $path = drupal_get_path_alias($_GET['q']);
      $classes[] = om_tools_class_safe('page-' . $path);
      // Add unique class for each website section.
      list($section) = explode('/', $path, 2);
			$arg = arg();
      if ($arg[0] == 'node') {
        $node_type = $vars['node']->type; 
        if (variable_get('om_tools_node_' . $node_type . '_titles', 0)) {
          unset($vars['title']);
          $classes[] = 'om-tools-no-title';
        }           
        if (variable_get('om_tools_node_body_classes', 1)) {
          if (($arg[1] == 'add') && !isset($arg[2])) {
            $section = 'node-add'; 
						                    //section-node-add
            $page_type = $arg[2];                      //-page
            $page_type_op = $arg[2] . '-add';          //-page-add    
          }
          if (($arg[1] == 'add') && isset($arg[2])) {
            $section = 'node-add';                     //section-node-add
            $page_type = $arg[2];                      //-page
            $page_type_op = $arg[2] . '-add';          //-page-add    
          }					
          elseif (is_numeric($arg[1]) && is_null($arg[2])) {
            $page_type = $node_type;                   //-page
            $page_type_op = $node_type . '-view';      //-page-view   
          }
          elseif (is_numeric($arg[1]) && ($arg[2] != 'edit') && ($arg[2] != 'delete')) {
            $page_type = $node_type;                   //-page
            $page_type_op = $node_type . '-view';      //-page-view   
          }					
          elseif (is_numeric($arg[1]) && ($arg[2] == 'edit' || $arg[2] == 'delete')) {
            $section = 'node-' . $arg[2];               //section-node-edit || section-node-delete
            $page_type = $node_type;                   //-page
            $page_type_op = $node_type . '-' . $arg[2]; //-page-edit || -page-delete       
          }  
          $classes[] = om_tools_class_safe(variable_get('om_tools_node_body_classes_prefix', 'content-type-') . $page_type);
          $classes[] = om_tools_class_safe(variable_get('om_tools_node_body_classes_prefix', 'content-type-') . $page_type_op);  
        }
        // Term classes
        if (!empty($vars['node']->taxonomy) && variable_get('om_tools_node_terms_body_classes', 1)) {
          foreach ($vars['node']->taxonomy as $term) {
            $classes[] = om_tools_class_safe(variable_get('om_tools_node_terms_body_classes_prefix', 'content-term-') . $term->name);
          }
        }
      }
      $classes[] = om_tools_class_safe('section-' . $section);
      $classes[] = 'om-tools-active';    
    }
    $vars['body_classes_array'] = $classes;
    $vars['body_classes'] = implode(' ', $classes); // Concatenate with spaces.	
    //dsm($vars);
  }
}
 
