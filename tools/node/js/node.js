$(document).ready(function(){
  om_set_switch('node');

	function om_set_switch(idname) {
    $('#edit-' + idname + '-tools-switch').change(
      function() {
        if ($('#edit-' + idname + '-tools-switch').attr('checked') == '') {
          $('#om-group-' + idname + '-settings').css('display', 'none');
        } else  {
          $('#om-group-' + idname + '-settings').css('display', 'block');
        }
      }
    );
    if ($('#edit-' + idname + '-tools-switch').attr('checked') == '') {
      $('#om-group-' + idname + '-settings').css('display', 'none');
    } else {
      $('#om-group-' + idname + '-settings').css('display', 'block');
    }	
	}
}); 
