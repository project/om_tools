<?php
/**
 * @file
 * User form alters
 */
 
/**
 * OM Tools User Form Settings
 *
 */
function om_user(&$form) {
  drupal_add_js(drupal_get_path('module', 'om_tools') . '/tools/user/js/user.js', 'module');
  // Reset User Login Block
  if ( variable_get('om_tools_user_login_block_reset', 0)) {
    variable_set('om_tools_user_login_block_label', 1);
    variable_set('om_tools_user_login_block_name_label_text', 'User Name');
    variable_set('om_tools_user_login_block_pass_label_text', 'Password');

    variable_set('om_tools_user_login_block_size', 15);
    variable_set('om_tools_user_login_block_name_hover', 'Enter your user name.');
    variable_set('om_tools_user_login_block_pass_hover', 'Enter your password.');
    variable_set('om_tools_user_login_block_button', 'Login');
    variable_set('om_tools_user_login_block_register', 'Create new account');
    variable_set('om_tools_user_login_block_password', 'Request new password');
    variable_set('om_tools_user_login_block_reset', 0);
  }
  // User Login Block
  $form['user_login_block_alter'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Login Block Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_alter_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Change User Login Form'),
    '#default_value' => variable_get('om_tools_user_login_block_alter_switch', 0),
    '#description' => t(''),
  );  
  $form['user_login_block_alter']['user_login_block_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-user-login-block-settings'),
    '#title' => t('User Login Block Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put label inside the boxes.'),
    '#default_value' => variable_get('om_tools_user_login_block_label', 1),
    '#description' => t(''),
  ); 
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_name_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Label Text'),
    '#default_value' => variable_get('om_tools_user_login_block_name_label_text', 'User Name'),
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_pass_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('User Password Label Text'),
    '#default_value' => variable_get('om_tools_user_login_block_pass_label_text', 'Password'),
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Search box size'),
    '#default_value' => variable_get('om_tools_user_login_block_size', 15),
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_name_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Rollover Text'),
    '#default_value' => variable_get('om_tools_user_login_block_name_hover', 'Enter your user name.'),
    '#description' => t(''),
  ); 
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_pass_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('User Password Rollover Text'),
    '#default_value' => variable_get('om_tools_user_login_block_pass_hover', 'Enter your password.'),
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#default_value' => variable_get('om_tools_user_login_block_button', 'Login'),
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_register'] = array(
    '#type' => 'textfield',
    '#title' => t('Register Text'),
    '#default_value' => variable_get('om_tools_user_login_block_register', 'Create new account'),
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_settings']['user_login_block_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password Request Text'),
    '#default_value' => variable_get('om_tools_user_login_block_password', 'Request new password'),
    '#description' => t(''),
  );
  $form['user_login_block_alter']['user_login_block_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),
    '#default_value' => variable_get('om_tools_user_login_block_reset', 0),
    '#description' => t(''),
  );
  // Reset User Login Page
  if ( variable_get('om_tools_user_login_reset', 0)) {
    variable_set('om_tools_user_login_title', 'Log In');

    variable_set('om_tools_user_login_label', 1);
    variable_set('om_tools_user_login_name_label_text', 'User Name');
    variable_set('om_tools_user_login_name_description', 'Enter your username.');
    variable_set('om_tools_user_login_pass_label_text', 'Password');
    variable_set('om_tools_user_login_pass_description', 'Enter the password that accompanies your username.');

    variable_set('om_tools_user_login_size', 30);
    variable_set('om_tools_user_login_name_hover', 'Enter your user name.');
    variable_set('om_tools_user_login_pass_hover', 'Enter your password.');
    variable_set('om_tools_user_login_button', 'Login');
    variable_set('om_tools_user_login_reset', 0);
  }
  // User Login Page
  $form['user_login_alter'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Login Page Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_alter_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Change User Login Page Form'),
    '#default_value' => variable_get('om_tools_user_login_alter_switch', 0),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-user-login-settings'),
    '#title' => t('User Login Page Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_title'] = array(
    '#type' => 'textfield',
    '#title' => t('User log in page title'),
    '#default_value' => variable_get('om_tools_user_login_title', 'Log In'),
    '#description' => t(''),
  ); 
  $form['user_login_alter']['user_login_settings']['user_login_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put label inside the boxes.'),
    '#default_value' => variable_get('om_tools_user_login_label', 1),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_name_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Label Text'),
    '#default_value' => variable_get('om_tools_user_login_name_label_text', 'User Name'),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_name_description'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Description'),
    '#default_value' => variable_get('om_tools_user_login_name_description', 'Enter your username.'),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_pass_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('User Password Label Text'),
    '#default_value' => variable_get('om_tools_user_login_pass_label_text', 'Password'),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_pass_description'] = array(
    '#type' => 'textfield',
    '#title' => t('User Password Description'),
    '#default_value' => variable_get('om_tools_user_login_pass_description', 'Enter the password that accompanies your username.'),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Text box size'),
    '#default_value' => variable_get('om_tools_user_login_size', 30),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_name_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Rollover Text'),
    '#default_value' => variable_get('om_tools_user_login_name_hover', 'Enter your user name.'),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_pass_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('User Password Rollover Text'),
    '#default_value' => variable_get('om_tools_user_login_pass_hover', 'Enter your password.'),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_settings']['user_login_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#default_value' => variable_get('om_tools_user_login_button', 'Login'),
    '#description' => t(''),
  );
  $form['user_login_alter']['user_login_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),
    '#default_value' => variable_get('om_tools_user_login_reset', 0),
    '#description' => t(''),
  );
  // User Reset Registration	
  if ( variable_get('om_tools_user_register_reset', 0)) {
    variable_set('om_tools_user_register_title', 'Register');

    variable_set('om_tools_user_register_label', 1);
    variable_set('om_tools_user_register_name_label_text', 'User Name');
    variable_set('om_tools_user_register_name_description', 'Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.');
    variable_set('om_tools_user_register_mail_label_text', 'E-mail Address');
    variable_set('om_tools_user_register_mail_description', 'A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.');

    variable_set('om_tools_user_register_size', 30);
    variable_set('om_tools_user_register_name_hover', 'Enter your user name.');
    variable_set('om_tools_user_register_mail_hover', 'Enter your e-mail address.');
    variable_set('om_tools_user_register_button', 'Create new account');
    variable_set('om_tools_user_register_reset', 0);
  }
  // User Registration Page
  $form['user_register_alter'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Register Page Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_alter_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Change User Register Page Form'),
    '#default_value' => variable_get('om_tools_user_register_alter_switch', 0),
    '#description' => t(''),
  );  
  $form['user_register_alter']['user_register_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-user-register-settings'),
    '#title' => t('User register Page Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );  
  $form['user_register_alter']['user_register_settings']['user_register_title'] = array(
    '#type' => 'textfield',
    '#title' => t('User register page title'),
    '#default_value' => variable_get('om_tools_user_register_title', 'Register'),
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_settings']['user_register_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put label inside the boxes.'),
    '#default_value' => variable_get('om_tools_user_register_label', 1),
    '#description' => t(''),
  ); 
  $form['user_register_alter']['user_register_settings']['user_register_name_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Label Text'),
    '#default_value' => variable_get('om_tools_user_register_name_label_text', 'User Name'),
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_settings']['user_register_name_description'] = array(
    '#type' => 'textarea',
    '#title' => t('User Name Description'),
    '#default_value' => variable_get('om_tools_user_register_name_description', 'Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.'),
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_settings']['user_register_mail_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('User E-mail Label Text'),
    '#default_value' => variable_get('om_tools_user_register_mail_label_text', 'E-mail Address'),
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_settings']['user_register_mail_description'] = array(
    '#type' => 'textarea',
    '#title' => t('User E-mail Description'),
    '#default_value' => variable_get('om_tools_user_register_mail_description', 'A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.'),
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_settings']['user_register_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Text box size'),
    '#default_value' => variable_get('om_tools_user_register_size', 30),
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_settings']['user_register_name_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name Rollover Text'),
    '#default_value' => variable_get('om_tools_user_register_name_hover', 'Enter your user name.'),
    '#description' => t(''),
  ); 
  $form['user_register_alter']['user_register_settings']['user_register_mail_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('User E-mail Rollover Text'),
    '#default_value' => variable_get('om_tools_user_register_mail_hover', 'Enter your e-mail address.'),
    '#description' => t(''),
  );
  $form['user_register_alter']['user_register_settings']['user_register_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#default_value' => variable_get('om_tools_user_register_button', 'Create new account'),
    '#description' => t(''),
  ); 
  $form['user_register_alter']['user_register_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),
    '#default_value' => variable_get('om_tools_user_register_reset', 0),
    '#description' => t(''),
  );

  // Reset New Request Password
  if ( variable_get('om_tools_user_pass_reset', 0)) {
    variable_set('om_tools_user_pass_title', 'New Password Request');

    variable_set('om_tools_user_pass_label', 1);
    variable_set('om_tools_user_pass_name_label_text', 'User Name or E-mail');
    variable_set('om_tools_user_pass_name_description', '');
    variable_set('om_tools_user_pass_size', 30);
    variable_set('om_tools_user_pass_name_hover', 'Enter your user name or e-mail.');
    variable_set('om_tools_user_pass_button', 'E-mail new password');
    variable_set('om_tools_user_pass_reset', 0);
  }
  // User Request for New Password Page
  $form['user_pass_alter'] = array(
    '#type' => 'fieldset',
    '#title' => t('User New Password Request Page Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['user_pass_alter']['user_pass_alter_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Change User New Password Request Page Form'),
    '#default_value' => variable_get('om_tools_user_pass_alter_switch', 0),
    '#description' => t(''),
  );  
  $form['user_pass_alter']['user_pass_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-user-pass-settings'),
    '#title' => t('User New Password Request Page Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );  
  $form['user_pass_alter']['user_pass_settings']['user_pass_title'] = array(
    '#type' => 'textfield',
    '#title' => t('User new password request page title'),
    '#default_value' => variable_get('om_tools_user_pass_title', 'New Password Request'),
    '#description' => t(''),
  );
  $form['user_pass_alter']['user_pass_settings']['user_pass_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put label inside the boxes.'),
    '#default_value' => variable_get('om_tools_user_pass_label', 1),
    '#description' => t(''),
  ); 
  $form['user_pass_alter']['user_pass_settings']['user_pass_name_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name or E-mail Label Text'),
    '#default_value' => variable_get('om_tools_user_pass_name_label_text', 'Username or E-mail Address'),
    '#description' => t(''),
  );
  $form['user_pass_alter']['user_pass_settings']['user_pass_name_description'] = array(
    '#type' => 'textarea',
    '#title' => t('User Name or E-mail Description'),
    '#default_value' => variable_get('om_tools_user_pass_name_description', ''),
    '#description' => t(''),
  );
  $form['user_pass_alter']['user_pass_settings']['user_pass_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Text box size'),
    '#default_value' => variable_get('om_tools_user_pass_size', 30),
    '#description' => t(''),
  );
  $form['user_pass_alter']['user_pass_settings']['user_pass_name_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name or E-mail Rollover Text'),
    '#default_value' => variable_get('om_tools_user_pass_name_hover', 'Enter your user name or e-mail.'),
    '#description' => t(''),
  );
  $form['user_pass_alter']['user_pass_settings']['user_pass_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#default_value' => variable_get('om_tools_user_pass_button', 'E-mail new password'),
    '#description' => t(''),
  );
  $form['user_pass_alter']['user_pass_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),
    '#default_value' => variable_get('om_tools_user_pass_reset', 0),
    '#description' => t(''),
  );
  return $form;
}


/**
 * OM Tools User Form Alters
 *
 */
function om_user_alter(&$form, &$form_state, $form_id) { 
  // Make sure the js are added only once
  static $user_login_block_js = 0;
  static $user_login_js = 0;
  static $user_register_js = 0;
  static $user_pass_js = 0; 

  switch ($form_id) {
    case "user_login_block":
      if ( variable_get('om_tools_user_login_block_alter_switch', 0)) {

        $user_login_block_label = variable_get('om_tools_user_login_block_label', 1);
        $user_login_block_name_label_text = variable_get('om_tools_user_login_block_name_label_text', 'User Name');
        $user_login_block_pass_label_text = variable_get('om_tools_user_login_block_pass_label_text', 'Password');

        $user_login_block_size = variable_get('om_tools_user_login_block_size', 15);
        $user_login_block_name_hover = variable_get('om_tools_user_login_block_name_hover', 'Enter your user name.');
        $user_login_block_pass_hover = variable_get('om_tools_user_login_block_pass_hover', 'Enter your password.');
        $user_login_block_button = variable_get('om_tools_user_login_block_button', 'Login');
        $user_login_block_register = variable_get('om_tools_user_login_block_register', 'Create new account');
        $user_login_block_password = variable_get('om_tools_user_login_block_password', 'Request new password');

        $user_login_block_links = '<div class="item-list"><ul><li class="first"><a href="' . base_path() . 'user/register" title="Create a new user account.">' . $user_login_block_register . '</a></li> <li class="last"><a href="' . base_path() . 'user/password" title="Request new password via e-mail.">' . $user_login_block_password . '</a></li> </ul></div>';

        if ($user_login_block_label) {
          unset($form['name']['#title']);
          unset($form['pass']['#title']);

          $form['name']['#default_value'] = $user_login_block_name_label_text;
          
          $user_login_block_js++;
          
          if ($user_login_block_js == 1) {
          
          drupal_add_js("
          $(document).ready(function(){
            $('#user-login-form input#edit-name').val('" . $user_login_block_name_label_text . "');
              $('#user-login-form input#edit-name').click(function() {
                if($(this).val() == '" . $user_login_block_name_label_text . "') { $(this).val(''); };
              });
              $('#user-login-form input#edit-name').blur(function() {
                if(($(this).val() == '" . $user_login_block_name_label_text . "') || ($(this).val() == '')) { 
                  $(this).val('" . $user_login_block_name_label_text . "'); 
                }
              });

              $('#user-login-form input#edit-pass').before(\"<input type='text' class='form-text' size='" . $user_login_block_size . "' maxlength='60' id='dummy-pass' name='dummy-pass' value='" . $user_login_block_pass_label_text . "' title='" . t($user_login_block_pass_hover) . "'>\");

              $('#user-login-form input#edit-pass').hide();
              $('#user-login-form input#dummy-pass').show();

              $('#user-login-form input#dummy-pass').focus(function() {
                $('#user-login-form input#dummy-pass').hide();
                $('#user-login-form input#edit-pass').show();
                $('#user-login-form input#edit-pass').focus();
              });
              $('#user-login-form input#edit-pass').blur(function() {
                if($('#user-login-form input#edit-pass').val() == '') {
                  $('#user-login-form input#dummy-pass').show();
                  $('#user-login-form input#edit-pass').hide();
                } 
             });
           });
         ", "inline");
         }
       }
       else {
         $form['name']['#title'] = $user_login_block_name_label_text;
         $form['pass']['#title'] = $user_login_block_pass_label_text;
       }
       $form['name']['#size'] = $user_login_block_size;
       $form['name']['#attributes'] = array('title' => t($user_login_block_name_hover));
       $form['pass']['#size'] = $user_login_block_size;
       $form['pass']['#attributes'] = array('title' => t($user_login_block_pass_hover));

       $form['submit'] = array('#type' => 'submit', '#value' => t($user_login_block_button));

       $form['links']['#value'] = $user_login_block_links; 
       break;
     }
   case "user_login":
     if (variable_get('om_tools_user_login_alter_switch', 0)) {
       drupal_set_title(t(variable_get('om_tools_user_login_title', 'Log In')));

       $user_login_label = variable_get('om_tools_user_login_label', 1);
       $user_login_name_label_text = variable_get('om_tools_user_login_name_label_text', 'User Name');
       $user_login_name_description = variable_get('om_tools_user_login_name_description', 'Enter your username.');
       $user_login_pass_label_text = variable_get('om_tools_user_login_pass_label_text', 'Password');
       $user_login_pass_description = variable_get('om_tools_user_login_pass_description', 'Enter the password that accompanies your username.');

       $user_login_size = variable_get('om_tools_user_login_size', 30);
       $user_login_name_hover = variable_get('om_tools_user_login_name_hover', 'Enter your user name.');
       $user_login_pass_hover = variable_get('om_tools_user_login_pass_hover', 'Enter your password.');
       $user_login_button = variable_get('om_tools_user_login_button', 'Login');

       if ($user_login_label) {
          unset($form['name']['#title']);
          unset($form['pass']['#title']);

          $form['name']['#default_value'] = $user_login_name_label_text;
          
          $user_login_js++;
          
          if ($user_login_js == 1) {          
          
          drupal_add_js("

          $(document).ready(function(){
            $('#user-login input#edit-name').val('" . $user_login_name_label_text . "');
              $('#user-login input#edit-name').click(function() {
              if($(this).val() == '" . $user_login_name_label_text . "') { $(this).val(''); };
            });
            $('#user-login input#edit-name').blur(function() {
              if(($(this).val() == '" . $user_login_name_label_text . "') || ($(this).val() == '')) { 
                 $(this).val('" . $user_login_name_label_text . "'); 
              }
            });

            $('#user-login input#edit-pass').before(\"<input type='text' class='form-text' size='" . $user_login_size . "' maxlength='60' id='dummy-pass' name='dummy-pass' value='" . $user_login_pass_label_text . "' title='" . t($user_login_pass_hover) . "'>\");

            $('#user-login input#edit-pass').hide();
            $('#user-login input#dummy-pass').show();

            $('#user-login input#dummy-pass').focus(function() {
                $('#user-login input#dummy-pass').hide();
                $('#user-login input#edit-pass').show();
                $('#user-login input#edit-pass').focus();
            });
            $('#user-login input#edit-pass').blur(function() {
               if($('#user-login input#edit-pass').val() == '') {
                 $('#user-login input#dummy-pass').show();
                 $('#user-login input#edit-pass').hide();
               } 
            });
          });
        ", "inline");
        }
      }
      else {
        $form['name']['#title'] = $user_login_name_label_text;
        $form['pass']['#title'] = $user_login_pass_label_text;

      }
      $form['name']['#description'] = $user_login_name_description;
      $form['name']['#size'] = $user_login_size;
      $form['name']['#attributes'] = array('title' => t($user_login_name_hover));
      $form['pass']['#description'] = $user_login_pass_description;
      $form['pass']['#size'] = $user_login_size;
      $form['pass']['#attributes'] = array('title' => t($user_login_pass_hover));

      $form['submit'] = array('#type' => 'submit', '#value' => t($user_login_button));
      break;
    }
    case "user_register":
      if (variable_get('om_tools_user_register_alter_switch', 0)) {
        drupal_set_title(t(variable_get('om_tools_user_register_title', 'Register')));

      $user_register_label = variable_get('om_tools_user_register_label', 1);
      $user_register_name_label_text = variable_get('om_tools_user_register_name_label_text', 'User Name');
      $user_register_name_description = variable_get('om_tools_user_register_name_description', 'Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.');
      $user_register_mail_label_text = variable_get('om_tools_user_register_mail_label_text', 'E-mail Address');
      $user_register_mail_description = variable_get('om_tools_user_register_mail_description', 'A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.');

      $user_register_size = variable_get('om_tools_user_register_size', 30);
      $user_register_name_hover = variable_get('om_tools_user_register_name_hover', 'Enter your user name.');
      $user_register_mail_hover = variable_get('om_tools_user_register_mail_hover', 'Enter your e-mail address.');
      $user_register_button = variable_get('om_tools_user_register_button', 'Create new account');

      if ($user_register_label) {
        unset($form['name']['#title']);
        unset($form['mail']['#title']);

        $form['name']['#default_value'] = $user_register_name_label_text;
        
        $user_register_js++;
          
       if ($user_register_js == 1) {          
        
        drupal_add_js("

        $(document).ready(function(){
          $('#user-register input#edit-name').val('" . $user_register_name_label_text . "');
            $('#user-register input#edit-name').click(function() {
              if($(this).val() == '" . $user_register_name_label_text . "') { $(this).val(''); };
            });
            $('#user-register input#edit-name').blur(function() {
              if(($(this).val() == '" . $user_register_name_label_text . "') || ($(this).val() == '')) { 
                 $(this).val('" . $user_register_name_label_text . "'); 
              }
            });

            $('#user-register input#edit-name-1').val('" . $user_register_name_label_text . "');
              $('#user-register input#edit-name-1').click(function() {
              if($(this).val() == '" . $user_register_name_label_text . "') { $(this).val(''); };
            });
            $('#user-register input#edit-name-1').blur(function() {
              if(($(this).val() == '" . $user_register_name_label_text . "') || ($(this).val() == '')) { 
                 $(this).val('" . $user_register_name_label_text . "'); 
              }
            });

            $('#user-register input#edit-mail').val('" . $user_register_mail_label_text . "');
              $('#user-register input#edit-mail').click(function() {
              if($(this).val() == '" . $user_register_mail_label_text . "') { $(this).val(''); };
            });
            $('#user-register input#edit-mail').blur(function() {
              if(($(this).val() == '" . $user_register_mail_label_text . "') || ($(this).val() == '')) { 
                 $(this).val('" . $user_register_mail_label_text . "'); 
              }
            });
          });
        ", "inline");
        }
      }
      else {
        $form['name']['#title'] = $user_register_name_label_text;
        $form['mail']['#title'] = $user_register_mail_label_text;
      }
      $form['name']['#description'] = $user_register_name_description;
      $form['name']['#size'] = $user_register_size;
      $form['name']['#attributes'] = array('title' => t($user_register_name_hover));
      $form['mail']['#description'] = $user_register_mail_description;
      $form['mail']['#size'] = $user_register_size;
      $form['mail']['#attributes'] = array('title' => t($user_register_mail_hover));

      $form['submit'] = array('#type' => 'submit', '#value' => t($user_register_button));
      break;
    }
    case "user_pass":
      if (variable_get('om_tools_user_pass_alter_switch', 0)) {
        drupal_set_title(t(variable_get('om_tools_user_pass_title', 'New Password Request')));

      $user_pass_label = variable_get('om_tools_user_pass_label', 1);
      $user_pass_name_label_text = variable_get('om_tools_user_pass_name_label_text', 'User Name or E-mail');
      $user_pass_name_description = variable_get('om_tools_user_pass_name_description', '');

      $user_pass_size = variable_get('om_tools_user_pass_size', 30);
      $user_pass_name_hover = variable_get('om_tools_user_pass_name_hover', 'Enter your user name or e-mail.');
      $user_pass_button = variable_get('om_tools_user_pass_button', 'E-mail new password');

      if ($user_pass_label) {
        unset($form['name']['#title']);
        unset($form['mail']['#title']);

        $form['name']['#default_value'] = $user_pass_name_label_text;
        
        $user_pass_js++;
          
        if ($user_pass_js == 1) {          
        
        drupal_add_js("
        $(document).ready(function(){
          $('#user-pass input#edit-name').val('" . $user_pass_name_label_text . "');
            $('#user-pass input#edit-name').click(function() {
              if($(this).val() == '" . $user_pass_name_label_text . "') { $(this).val(''); };
            });
            $('#user-pass input#edit-name').blur(function() {
              if(($(this).val() == '" . $user_pass_name_label_text . "') || ($(this).val() == '')) { 
                 $(this).val('" . $user_pass_name_label_text . "'); 
              }
            });
          });
        ", "inline");
        }
      }
      else {
        $form['name']['#title'] = $user_pass_name_label_text;
      }
      $form['name']['#description'] = $user_pass_name_description;
      $form['name']['#size'] = $user_pass_size;
      $form['name']['#attributes'] = array('title' => t($user_pass_name_hover));
      $form['submit'] = array('#type' => 'submit', '#value' => t($user_pass_button));
      break;
    }
  }
}

