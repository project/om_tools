$(document).ready(function(){
  om_set_switch('user-login-block');
  om_set_switch('user-login');
  om_set_switch('user-register');
  om_set_switch('user-pass');

	function om_set_switch(idname) {
    $('#edit-' + idname + '-alter-switch').change(
      function() {
        if ($('#edit-' + idname + '-alter-switch').attr('checked') == '') {
          $('#om-group-' + idname + '-settings').css('display', 'none');
        } else  {
          $('#om-group-' + idname + '-settings').css('display', 'block');
        }
      }
    );
    if ($('#edit-' + idname + '-alter-switch').attr('checked') == '') {
      $('#om-group-' + idname + '-settings').css('display', 'none');
    } else {
      $('#om-group-' + idname + '-settings').css('display', 'block');
    }	
	}
}); 
