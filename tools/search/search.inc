<?php
/**
 * @file
 * Search form alters
 */
 
/**
 * OM Tools Search Form Settings
 *
 */
function om_search(&$form) {
  drupal_add_js(drupal_get_path('module', 'om_tools') . '/tools/search/js/search.js', 'module');

  // Reset Search Theme Form
  if ( variable_get('om_tools_search_theme_reset', 0)) {
    variable_set('om_tools_search_theme_label', 1);
    variable_set('om_tools_search_theme_label_text', 'Search this site');  
    variable_set('om_tools_search_theme_size', 15);
    variable_set('om_tools_search_theme_hover', 'Enter the terms you wish to search for.');
    variable_set('om_tools_search_theme_button', 'Go');
    variable_set('om_tools_search_theme_reset', 0);
  }  
  // Search Theme Form
  $form['search_theme_alter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Theme Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['search_theme_alter']['search_theme_alter_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Change Search Theme Form'),
    '#default_value' => variable_get('om_tools_search_theme_alter_switch', 0),
    '#description' => t(''),
  );  
  $form['search_theme_alter']['search_theme_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-search-theme-settings'),
    '#title' => t('Search Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );  
  $form['search_theme_alter']['search_theme_settings']['search_theme_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put label inside the search box.'),
    '#default_value' => variable_get('om_tools_search_theme_label', 1),
    '#description' => t(''),
  ); 
  $form['search_theme_alter']['search_theme_settings']['search_theme_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Label Text'),    
    '#default_value' => variable_get('om_tools_search_theme_label_text', 'Search this site'),
    '#description' => t(''),
  );    
  $form['search_theme_alter']['search_theme_settings']['search_theme_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Search box size'),
    '#default_value' => variable_get('om_tools_search_theme_size', 15),
    '#description' => t(''),
  );
  $form['search_theme_alter']['search_theme_settings']['search_theme_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('Rollover Text'),        
    '#default_value' => variable_get('om_tools_search_theme_hover', 'Enter the terms you wish to search for.'),
    '#description' => t(''),
  );   
  $form['search_theme_alter']['search_theme_settings']['search_theme_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),      
    '#default_value' => variable_get('om_tools_search_theme_button', 'Go'),
    '#description' => t(''),
  );   
  $form['search_theme_alter']['search_theme_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),      
    '#default_value' => variable_get('om_tools_search_theme_reset', 0),
    '#description' => t(''),
  );   
  // Reset Search Block 
  if ( variable_get('om_tools_search_block_reset', 0)) {
    variable_set('om_tools_search_block_label', 1);
    variable_set('om_tools_search_block_label_text', 'Search this site');  
    variable_set('om_tools_search_block_size', 15);
    variable_set('om_tools_search_block_hover', 'Enter the terms you wish to search for.');
    variable_set('om_tools_search_block_button', 'Go');
    variable_set('om_tools_search_block_reset', 0);
  }  
  // Search Block Form
  $form['search_block_alter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Block Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['search_block_alter']['search_block_alter_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Change Search Block Form'),
    '#default_value' => variable_get('om_tools_search_block_alter_switch', 0),
    '#description' => t(''),
  );  
  $form['search_block_alter']['search_block_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-search-block-settings'),
    '#title' => t('Search Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );  
  $form['search_block_alter']['search_block_settings']['search_block_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put label inside the search box.'),
    '#default_value' => variable_get('om_tools_search_block_label', 1),
    '#description' => t(''),
  ); 
  $form['search_block_alter']['search_block_settings']['search_block_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Label Text'),    
    '#default_value' => variable_get('om_tools_search_block_label_text', 'Search this site'),
    '#description' => t(''),
  );    
  $form['search_block_alter']['search_block_settings']['search_block_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Search box size'),
    '#default_value' => variable_get('om_tools_search_block_size', 15),
    '#description' => t(''),
  );
  $form['search_block_alter']['search_block_settings']['search_block_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('Rollover Text'),        
    '#default_value' => variable_get('om_tools_search_block_hover', 'Enter the terms you wish to search for.'),
    '#description' => t(''),
  );   
  $form['search_block_alter']['search_block_settings']['search_block_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),      
    '#default_value' => variable_get('om_tools_search_block_button', 'Go'),
    '#description' => t(''),
  );   
  $form['search_block_alter']['search_block_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),      
    '#default_value' => variable_get('om_tools_search_block_reset', 0),
    '#description' => t(''),
  );     
  // Reset Search Page 
  if ( variable_get('om_tools_search_reset', 0)) {
    variable_set('om_tools_search_title', 'Search');

    variable_set('om_tools_search_label', 1);
    variable_set('om_tools_search_label_text', 'Enter your keywords');  
    variable_set('om_tools_search_size', 30);
    variable_set('om_tools_search_hover', 'Enter the terms you wish to search for.');
    variable_set('om_tools_search_button', 'Go');
    variable_set('om_tools_search_reset', 0);
  }  
  // Search Page
  $form['search_alter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Page Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t(''),
  );
  $form['search_alter']['search_alter_switch'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Change Search Page Form'),
    '#default_value' => variable_get('om_tools_search_alter_switch', 0),
    '#description' => t(''),
  );  
  $form['search_alter']['search_settings'] = array(
    '#type' => 'fieldset',
    '#attributes' => array( 'id' => 'om-group-search-settings'),
    '#title' => t('Search Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t(''),
  );  
  $form['search_alter']['search_settings']['search_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Search page title'),
    '#default_value' => variable_get('om_tools_search_title', 'Search'),
    '#description' => t(''),
  ); 
  $form['search_alter']['search_settings']['search_label'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put label inside the search box.'),
    '#default_value' => variable_get('om_tools_search_label', 1),
    '#description' => t(''),
  ); 
  $form['search_alter']['search_settings']['search_label_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Label Text'),    
    '#default_value' => variable_get('om_tools_search_label_text', 'Enter your keywords'),
    '#description' => t(''),
  );    
  $form['search_alter']['search_settings']['search_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Search box size'),
    '#default_value' => variable_get('om_tools_search_size', 30),
    '#description' => t(''),
  );
  $form['search_alter']['search_settings']['search_hover'] = array(
    '#type' => 'textfield',
    '#title' => t('Rollover Text'),        
    '#default_value' => variable_get('om_tools_search_hover', 'Enter the terms you wish to search for.'),
    '#description' => t(''),
  );   
  $form['search_alter']['search_settings']['search_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),      
    '#default_value' => variable_get('om_tools_search_button', 'Go'),
    '#description' => t(''),
  );   
  $form['search_alter']['search_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset to default values.'),      
    '#default_value' => variable_get('om_tools_search_reset', 0),
    '#description' => t(''),
  );                                                 
  return $form;  
}


/**
 * OM Tools Search Form Alters
 *
 */
function om_search_alter(&$form, &$form_state, $form_id) {
  // Make sure the js are added only once
  static $search_form_js = 0;
  static $search_theme_js = 0;
  static $search_block_js = 0;
  
  switch ($form_id) {
    case "search_form":  
      if ( variable_get('om_tools_search_alter_switch', 0)) {
        drupal_set_title(t(variable_get('om_tools_search_title', 'Search')));

        $search_label = variable_get('om_tools_search_label', 1);
        $search_label_text = variable_get('om_tools_search_label_text', 'Enter your keywords');  
        $search_size = variable_get('om_tools_search_size', 30);
        $search_hover = variable_get('om_tools_search_hover', 'Enter the terms you wish to search for.');
        $search_button = variable_get('om_tools_search_button', 'Go');
    
        if ($search_label) {
          unset($form['basic']['#title']);
          $form['basic']['inline']['keys']['#default_value'] = $search_label_text;
          
          $search_form_js++;
          
          if ($search_form_js == 1) {                    
          drupal_add_js("
            $(document).ready(function(){
              $('form#search-form input#edit-keys').val('" . $search_label_text . "');
              $('form#search-form input#edit-keys').click(function() {
                if ($(this).val() == '" . $search_label_text . "') { 
                  $(this).val(''); 
                };
              });
              $('form#search-form input#edit-keys').blur( function() {
                if (($(this).val() == '" . $search_label_text . "') || ($(this).val() == '')) {
                  $(this).val('" . $search_label_text . "');
                };
              });    
            }); 
          ", "inline");
          }
        }
        else {
          $form['basic']['#title'] = $search_label_text;
        }  
        $form['basic']['inline']['keys']['#size'] = $search_size;
        $form['basic']['inline']['keys']['#attributes'] = array('title' => t($search_hover));
        $form['basic']['inline']['submit']['#value'] = t($search_button);
        break;
    }
    
    case "search_theme_form":
      if ( variable_get('om_tools_search_theme_alter_switch', 0)) {
        $search_theme_label = variable_get('om_tools_search_theme_label', 1);
        $search_theme_label_text = variable_get('om_tools_search_theme_label_text', 'Search this site');  
        $search_theme_size = variable_get('om_tools_search_theme_size', 15);
        $search_theme_hover = variable_get('om_tools_search_theme_hover', 'Enter the terms you wish to search for.');
        $search_theme_button = variable_get('om_tools_search_theme_button', 'Go');
    
        if ($search_theme_label) {
          unset($form['search_theme_form']['#title']);
          $form['search_theme_form']['#default_value'] = $search_theme_label_text;
       
          $search_theme_js++;
          
          if ($search_theme_js == 1) {          
          
          drupal_add_js("
            $(document).ready(function(){
              $('form#search-theme-form input.form-text').val('" . $search_theme_label_text . "');
              $('form#search-theme-form input.form-text').click(function() {
                if ($(this).val() == '" . $search_theme_label_text . "') { 
                  $(this).val(''); 
                };
              });
              $('form#search-theme-form input.form-text').blur( function() {
                if (($(this).val() == '" . $search_theme_label_text . "') || ($(this).val() == '')) {
                  $(this).val('" . $search_theme_label_text . "');
                };
              });    
            }); 
          ", "inline");
          }
        }
        else {
          $form['search_theme_form']['#title'] = $search_theme_label_text;
        }  
        $form['search_theme_form']['#size'] = $search_theme_size;
        $form['search_theme_form']['#attributes'] = array('title' => t($search_theme_hover));
        $form['submit']['#value'] = t($search_theme_button);
        break;
    }
    case "search_block_form":  
      if ( variable_get('om_tools_search_block_alter_switch', 0)) {
        $search_block_label = variable_get('om_tools_search_block_label', 1);
        $search_block_label_text = variable_get('om_tools_search_block_label_text', 'Search this site');  
        $search_block_size = variable_get('om_tools_search_block_size', 15);
        $search_block_hover = variable_get('om_tools_search_block_hover', 'Enter the terms you wish to search for.');
        $search_block_button = variable_get('om_tools_search_block_button', 'Go');
    
        if ($search_block_label) {
          unset($form['search_block_form']['#title']);
          $form['search_block_form']['#default_value'] = $search_block_label_text;
          
          $search_block_js++;
          
          if ($search_block_js == 1) {          
          
          drupal_add_js("
            $(document).ready(function(){
              $('form#search-block-form input.form-text').val('" . $search_block_label_text . "');
              $('form#search-block-form input.form-text').click(function() {
                if ($(this).val() == '" . $search_block_label_text . "') { 
                  $(this).val(''); 
                };
              });
              $('form#search-block-form input.form-text').blur( function() {
                if (($(this).val() == '" . $search_block_label_text . "') || ($(this).val() == '')) {
                  $(this).val('" . $search_block_label_text . "');
                };
              });    
            }); 
          ", "inline");
          }
        }
        else {
          $form['search_block_form']['#title'] = $search_block_label_text;
        }  
        $form['search_block_form']['#size'] = $search_block_size;
        $form['search_block_form']['#attributes'] = array('title' => t($search_block_hover));
        $form['submit']['#value'] = t($search_block_button);
        break;
    }    
  }
}

