$(document).ready(function(){
  om_set_switch('search-theme');
  om_set_switch('search-block');
  om_set_switch('search');

	function om_set_switch(idname) {
    $('#edit-' + idname + '-alter-switch').change(
      function() {
        if ($('#edit-' + idname + '-alter-switch').attr('checked') == '') {
          $('#om-group-' + idname + '-settings').css('display', 'none');
        } else  {
          $('#om-group-' + idname + '-settings').css('display', 'block');
        }
      }
    );
    if ($('#edit-' + idname + '-alter-switch').attr('checked') == '') {
      $('#om-group-' + idname + '-settings').css('display', 'none');
    } else {
      $('#om-group-' + idname + '-settings').css('display', 'block');
    }	
	}
}); 
